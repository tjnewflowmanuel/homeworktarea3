package com.example.homework3_storageedilberto

import androidx.appcompat.app.AppCompatActivity
import com.example.homework3_storageedilberto.model.EncuestaRepository

abstract class BaseActivity:AppCompatActivity() {

    protected val repository by lazy {
        EncuestaRepository(AppExecutors(),
            DBDataSource(applicationContext))
    }
}