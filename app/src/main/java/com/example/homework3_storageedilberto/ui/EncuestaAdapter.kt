package com.example.homework3_storageedilberto.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.homework3_storageedilberto.R
import com.example.homework3_storageedilberto.model.Encuesta
import kotlinx.android.synthetic.main.row_note.view.*

class  EncuestaAdapter(private var encuestas:List<Encuesta>,
                       private val itemCallback:(encuesta: Encuesta)->Unit?):RecyclerView.Adapter<
        EncuestaAdapter.EncuestaViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EncuestaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_encuesta,
            parent,false)
        return EncuestaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return encuestas.size
    }

    override fun onBindViewHolder(holder: EncuestaViewHolder, position: Int) {
        holder.bind(encuestas[position])
        holder.view.setOnClickListener {
            itemCallback(encuestas[position])
        }
    }

    fun update(data:List<Encuesta>){
        encuestas= data
        notifyDataSetChanged()
    }

    class EncuestaViewHolder(val view:View):RecyclerView.ViewHolder(view){

        fun bind(encuesta: Encuesta){
            view.tviName.text = encuesta.name
        }
    }
}