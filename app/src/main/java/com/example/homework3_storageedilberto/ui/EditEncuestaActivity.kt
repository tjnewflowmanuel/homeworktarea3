package com.example.homework3_storageedilberto.ui

import android.os.Bundle
import com.example.homework3_storageedilberto.BaseActivity
import com.example.homework3_storageedilberto.R
import com.example.homework3_storageedilberto.model.Encuesta
import com.example.homework3_storageedilberto.model.EncuestaRepository
import kotlinx.android.synthetic.main.activity_add_encuesta.eteDireccionEnc
import kotlinx.android.synthetic.main.activity_add_encuesta.eteDescripcionEnc
import kotlinx.android.synthetic.main.activity_add_encuesta.eteNameEnc
import kotlinx.android.synthetic.main.activity_add_encuesta.txtresulidealEnc
import kotlinx.android.synthetic.main.activity_edit_encuesta.*

class EditEncuestaActivity : BaseActivity(), EncuestaDialogFragment.DialogListener {

    private lateinit var encuestaRepository: EncuestaRepository
    private var encuestita: Encuesta?=null
    private var name:String?=null
    private var direccion:String?=null
    private var descripcion:String?=null
    private var resulideal:String?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_encuesta)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        verifyExtras()
        populate()

        ui()
    }
    private fun ui(){
        btnEditNote.setOnClickListener {
            if(validateForm()){
                editEnc()
            }
        }

        btnDeleteNote.setOnClickListener {
            showAluDialog()
        }
    }

    private fun editEnc(){
        val noteId= encuestita?.id
        val nEncuesta= Encuesta(noteId,name,direccion,descripcion,resulideal)
        repository.update(nEncuesta)
        finish()
    }

    private fun validateForm():Boolean{
        name= eteNameEnc.text.toString()
        direccion= eteDireccionEnc.text.toString()
        descripcion=eteDescripcionEnc.text.toString()
        resulideal=txtresulidealEnc.text.toString()
        if(name.isNullOrEmpty()){
            return false
        }
        if(direccion.isNullOrEmpty()){
            return false
        }
        if (descripcion.isNullOrEmpty()){
            return false
        }

        return true
    }

    private fun populate(){
        encuestita?.let {
            eteNameEnc.setText(it.name)
            eteDireccionEnc.setText(it.direccion)
            eteDescripcionEnc.setText(it.descripcion)
            txtresulidealEnc.text = it.resulideal
        }
    }

    private fun showAluDialog(){
        val noteDialogFragment= EncuestaDialogFragment()
        val bundle= Bundle()
        bundle.putString("TITLE","¿Deseas eliminar esta Encuesta?")
        bundle.putInt("TYPE",100)

        noteDialogFragment.arguments= bundle
        noteDialogFragment.show(supportFragmentManager,"dialog")
    }

    override fun onPositiveListener(any: Any?, type: Int) {
        encuestita?.let {
            repository.delete(it)
        }
        finish()
    }

    override fun onNegativeListener(any: Any?, type: Int) {}

    private fun verifyExtras(){
        intent?.extras?.let {
            encuestita= it.getSerializable("ENCUESTA") as Encuesta
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
