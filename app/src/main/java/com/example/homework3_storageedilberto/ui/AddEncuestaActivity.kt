package com.example.homework3_storageedilberto.ui

import android.os.Bundle
import android.widget.Toast
import com.example.homework3_storageedilberto.BaseActivity
import com.example.homework3_storageedilberto.R
import com.example.homework3_storageedilberto.model.Encuesta
import kotlinx.android.synthetic.main.activity_add_encuesta.*
import kotlinx.android.synthetic.main.activity_add_encuesta.btnAddEnc
import kotlinx.android.synthetic.main.activity_add_encuesta.eteNameEnc

class AddEncuestaActivity :BaseActivity() {
    private var name: String? = null
    private var direccion: String? = null
    private var descripcion: String? = null
    private var resulideal:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_encuesta)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        ui()
    }


    private fun ui() {
        btnAddEnc.setOnClickListener {
            if (validateForm()) {
                addEncuesta()
                finish()
            }
        }
    }

    private fun validateForm(): Boolean {
        clearForm()
        name = eteNameEnc.text.toString().trim()
        direccion = eteDireccionEnc.text.toString().trim()
        descripcion = eteDescripcionEnc.text.toString().trim()

        if (descripcion.toString().trim() == "" ) txtresulidealEnc.text = "Correcto" else txtresulidealEnc.text = "Incorrecto"
        if (direccion.toString().trim() == "" ) Toast.makeText(this, "Correcto", Toast.LENGTH_SHORT).show()
        if (name.isNullOrEmpty()) {
            eteNameEnc.error = "Campo nombre vacio rellene datos"
            return false
        }

        if (direccion.isNullOrEmpty()) {
            eteDireccionEnc.error = "Campo direccion vacio rellene datos"
            return false
        }

        if (descripcion.isNullOrEmpty()) {
            eteDescripcionEnc.error = "Campo descripcion vacio rellene datos"
            return false
        }

        return true
    }

    private fun clearForm() {
        eteNameEnc.error = null
        eteDireccionEnc.error = null
        eteDescripcionEnc.error = null
        txtresulidealEnc.error=null
    }

    private fun addEncuesta() {
        val encuesta = Encuesta(null, name, direccion, descripcion,resulideal)
        repository.add(encuesta) {
            Toast.makeText(this, "Correcto", Toast.LENGTH_SHORT).show()
        }


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}