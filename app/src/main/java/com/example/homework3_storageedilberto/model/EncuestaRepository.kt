package com.example.homework3_storageedilberto.model

import com.example.homework3_storageedilberto.AppExecutors
import com.example.homework3_storageedilberto.storage.DataSource
import com.example.homework3_storageedilberto.storage.EncuestaCallback
import com.example.homework3_storageedilberto.storage.Mapper

class EncuestaRepository(private val appExecutors: AppExecutors,
                         private val dataSource: DataSource
) {

    fun getAllWithCallback(callback:EncuestaCallback<Encuesta>){
        appExecutors.diskIO.execute {
            val encuestaList = dataSource.encuestas().map {
                Encuesta(it.id, it.name, it.direccion,it.descripcion,it.resulideal)
            }
            appExecutors.mainThread.execute {
                callback.success(encuestaList)
            }
        }
    }

    fun getAll(callback: (encuestas: List<Encuesta>) -> Unit) {
        appExecutors.diskIO.execute {
            val encuestaList = dataSource.encuestas().map {
                Encuesta(it.id, it.name, it.direccion,it.descripcion,it.resulideal)
            }
            appExecutors.mainThread.execute {
                callback(encuestaList)
            }
        }
    }

    fun add(encuesta: Encuesta, callback: () -> Unit) {
        appExecutors.diskIO.execute {
            dataSource.addEncuesta(Mapper.encuestaToDbEncuesta(encuesta))
            appExecutors.mainThread.execute {
                callback()
            }
        }
    }

    fun update(encuesta: Encuesta) {
        appExecutors.diskIO.execute {
            dataSource.updateEncuesta(Mapper.encuestaToDbEncuesta(encuesta))
        }
    }

    fun delete(encuesta: Encuesta) {
        appExecutors.diskIO.execute {
            dataSource.deleteEncuesta(Mapper.encuestaToDbEncuesta(encuesta))
        }
    }

}