package com.example.homework3_storageedilberto.model

import java.io.Serializable

data class Encuesta(val id:Int?, val name:String?, val direccion: String?, val descripcion: String?,val resulideal:String?):Serializable{

    override fun toString(): String {
        return "Encuesta(id=$id, name=$name, direccion=$direccion, descripcion=$descripcion,resulideal=$resulideal)"
    }


}