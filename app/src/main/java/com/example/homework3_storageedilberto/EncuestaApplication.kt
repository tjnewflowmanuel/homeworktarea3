package com.example.homework3_storageedilberto

import android.app.Application
import com.example.homework3_storageedilberto.model.EncuestaRepository

class EncuestaApplication:Application() {
    private lateinit var repository: EncuestaRepository
    override fun onCreate() {
        super.onCreate()

        repository = EncuestaRepository(
                AppExecutors(),
                DBDataSource(this))
    }
    fun provideRepository()= repository
}