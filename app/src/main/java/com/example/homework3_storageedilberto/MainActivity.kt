package com.example.homework3_storageedilberto

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework3_storageedilberto.model.Encuesta
import com.example.homework3_storageedilberto.ui.AddEncuestaActivity
import com.example.homework3_storageedilberto.ui.EditEncuestaActivity
import com.example.homework3_storageedilberto.ui.EncuestaAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private lateinit var adapter: EncuestaAdapter

    private val appRepository by lazy {
        (application as EncuestaApplication).provideRepository()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ui()
    }

    override fun onResume() {
        super.onResume()
        loadEnc()
    }

    private fun ui(){
        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter = EncuestaAdapter(emptyList()) { itEncuesta->
            showMessage("item ${itEncuesta.name}")
            goToEncuesta(itEncuesta)
        }

        recyclerView.adapter = adapter

        floatingActionButton.setOnClickListener {
            goToAddEncuesta()
        }
    }

    private fun goToAddEncuesta() {

        startActivity(Intent(this, AddEncuestaActivity::class.java))
    }
    private fun goToEncuesta(encuesta: Encuesta) {

        val bundle= Bundle()
        bundle.putSerializable("ENCUESTA",encuesta)
        val intent= Intent(this, EditEncuestaActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, "item $message", Toast.LENGTH_SHORT).show()
    }


    private fun loadEnc(){
        appRepository.getAll {
            adapter.update(it)
    }

}}
