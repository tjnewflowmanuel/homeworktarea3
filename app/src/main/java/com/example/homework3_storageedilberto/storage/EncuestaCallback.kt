package com.example.homework3_storageedilberto.storage

import androidx.annotation.MainThread

interface EncuestaCallback<T> {

    @MainThread
    fun success(data:List<T>)

    @MainThread
    fun success(){}

    @MainThread
    fun error (exception: Exception?)
}