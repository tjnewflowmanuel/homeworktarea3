package com.example.homework3_storageedilberto.storage.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_encuestas")
data class DBEncuesta(@PrimaryKey(autoGenerate = true)val id:Int?,
                        @ColumnInfo(name = "name") val name:String?,
                        @ColumnInfo(name = "direccion") val direccion:String?,
                        @ColumnInfo(name = "descripcion") val descripcion:String?,
                        @ColumnInfo(name = "resulideal") val resulideal:String?)

{

    override fun toString(): String {
        return "DBEncuesta(id=$id, name=$name, direccion=$direccion,descripcion=$descripcion,resulideal=$resulideal)"
    }
}