package com.example.homework3_storageedilberto.storage.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [DBEncuesta::class], version = 1)
abstract class EncuestaDataBase : RoomDatabase() {

    abstract fun encuestaDao(): EncuestaDao

    companion object {
        private var INSTANCE: EncuestaDataBase? = null
        private const val DBNAME="BDRoom.db"

        fun getInstance(context: Context): EncuestaDataBase? {
            if (INSTANCE == null) {
                synchronized(EncuestaDataBase::class) {
                    INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            EncuestaDataBase::class.java, DBNAME
                    )
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}