package com.example.homework3_storageedilberto.storage

import com.example.homework3_storageedilberto.storage.db.DBEncuesta



interface DataSource {

    fun encuestas():List<DBEncuesta>
    fun addEncuesta(encuesta: DBEncuesta)
    fun updateEncuesta(encuesta: DBEncuesta)
    fun deleteEncuesta(encuesta: DBEncuesta)
}