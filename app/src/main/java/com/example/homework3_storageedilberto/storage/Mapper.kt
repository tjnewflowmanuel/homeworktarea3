package com.example.homework3_storageedilberto.storage

import com.example.homework3_storageedilberto.model.Encuesta
import com.example.homework3_storageedilberto.storage.db.DBEncuesta

object Mapper {

    fun encuestaDbToEncuesta(dbEncuesta: DBEncuesta): Encuesta {
        return Encuesta(dbEncuesta.id,dbEncuesta.name,dbEncuesta.direccion,dbEncuesta.descripcion,dbEncuesta.resulideal)
    }

    fun encuestaToDbEncuesta(encuesta: Encuesta): DBEncuesta {
        return DBEncuesta(encuesta.id, encuesta.name,encuesta.direccion,encuesta.descripcion,encuesta.resulideal)
    }

    fun mapList(encuestaList: List<Encuesta>):List<DBEncuesta>{
        return encuestaList.map { encuestaToDbEncuesta(it) }
    }

    fun mapDbList(encuestaDBList: List<DBEncuesta>):List<Encuesta>{
        return encuestaDBList.map { encuestaDbToEncuesta(it) }
    }
}