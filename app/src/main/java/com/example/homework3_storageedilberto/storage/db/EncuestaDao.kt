package com.example.homework3_storageedilberto.storage.db

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.OnConflictStrategy.IGNORE


@Dao
interface EncuestaDao {

    @Query("SELECT * from tb_encuestas")
    fun encuestas(): List<DBEncuesta>

    @Insert(onConflict = REPLACE)
    fun addEncuesta(encuesta: DBEncuesta)

    @Update(onConflict = REPLACE)
    fun updateEncuesta(encuesta: DBEncuesta)

    @Delete
    fun deleteEncuesta(encuesta: DBEncuesta)

    @Query("DELETE from tb_encuestas")
    fun deleteAll()
}