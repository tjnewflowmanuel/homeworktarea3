package com.example.homework3_storageedilberto

import android.content.Context
import com.example.homework3_storageedilberto.storage.DataSource
import com.example.homework3_storageedilberto.storage.db.EncuestaDao
import com.example.homework3_storageedilberto.storage.db.EncuestaDataBase
import com.example.homework3_storageedilberto.storage.db.DBEncuesta


class DBDataSource(context:Context):DataSource{

    private lateinit var encuestaDao:EncuestaDao

    init{
        val db = EncuestaDataBase.getInstance(context)
        db?.let {
            encuestaDao = it.encuestaDao()
        }
    }
    override fun encuestas(): List<DBEncuesta> = encuestaDao.encuestas()

    override fun addEncuesta(encuesta: DBEncuesta) {
        encuestaDao.addEncuesta(encuesta)
    }

    override fun updateEncuesta(encuesta: DBEncuesta) {
        encuestaDao.updateEncuesta(encuesta)
    }

    override fun deleteEncuesta(encuesta: DBEncuesta) {
       encuestaDao.deleteEncuesta(encuesta)
    }

}